#version 120

uniform sampler3D colorTexture3;
varying vec3 N;
varying vec4 pos;

void main()
{
  vec3 texcoord3 = gl_TexCoord[0].xyz;//get the 3d Coord
  vec4 color = texture3D(colorTexture3, texcoord3);//Read color from texture3d
  vec3 mylightDir = normalize(vec3(0.1, 0.1, 0) - pos.xyz);//light position in ViewCoord
  vec3 myN = normalize(N);//This step is so important that the Rasteration step will use interpolattion to get N, which must be normalized.
  
  vec3 ReflectedRay = reflect(mylightDir, myN );
  vec3 CamDir = normalize(pos.xyz);//Cam position in ViewCoord is 0,0,0
  // color below = ambient + specular
  gl_FragColor.xyz = vec3(0.1,0.05,0.0) + 0.5 * color.xyz + 1 * color.xyz * clamp(dot(CamDir,ReflectedRay),-0.2,1.0);//dot( N,viewVector );//dot(viewVector,N);//color * 0.5;
}

